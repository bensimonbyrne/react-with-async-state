const path = require('path');

const webpack = require('webpack');

export default () => ({
  entry: './src/index.jsx',
  target: 'node',
  externals: {
    lodash: {
      commonjs: 'lodash',
      commonjs2: 'lodash',
      amd: 'lodash',
      root: '_',
    },
    react: {
      commonjs: 'react',
      commonjs2: 'react',
      amd: 'react',
      root: 'react',
    },
    'hoist-non-react-statics': {
      commonjs: 'hoist-non-react-statics',
      commonjs2: 'hoist-non-react-statics',
      amd: 'hoist-non-react-statics',
      root: 'hoist-non-react-statics',
    },
    'prop-types': {
      commonjs: 'prop-types',
      commonjs2: 'prop-types',
      amd: 'prop-types',
      root: 'prop-types',
    },
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({sourceMap: true}),
    new webpack.LoaderOptionsPlugin({minimize: true}),
  ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [
          {
            loader: 'eslint-loader',
            options: {
              formatter: require('eslint-friendly-formatter'),
              quiet: false,
              failOnError: true,
              failOnWarning: true,
              emitError: true,
              emitWarning: true,
            },
          },
        ],
        exclude: /node_modules/,
        enforce: 'pre',
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        include: [path.resolve(process.cwd(), 'src')],
        use: [
          {
            loader: 'babel-loader',
            query: {
              presets: ['es2015', 'react', 'stage-2'],
              // plugins: ['add-module-exports'],
            },
          },
        ],
      },
    ],
  },
  output: {
    library: 'reactWithAsyncState',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, 'lib'),
    filename: 'react-with-async-state.js',
  },
});
