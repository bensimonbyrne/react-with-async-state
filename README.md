React `withAsyncState()`
=======


A React high-order Component designed to abstract the fetch, wait, check flow of remote or asynchronous requests or Redux Thunk actions. The goal is to make it a drop in component that connects to the store, rendering the `TargetComponent` only when the state is in shape.


## Install

`$ npm install react-with-async-state --save`


## API

`withAsyncState( Component, Wait, Error, Function )`


## Usage

```javascript

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  withAsyncState(
    AComponentThatNeedsSomethingBeforeRendering,
    Loader,
    ErrorMessage,
    (props) => !!_.size(props.items)
  )
);


```
