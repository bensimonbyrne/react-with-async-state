import _ from 'lodash';


/**
 * hoistProps - Description
 *
 * @param {type} srcComponent Description
 * @param {type} Component    Description
 *
 * @return {type} Description
 */
export default function hoistProps(srcComponent, Component) {
  return _.pick(srcComponent.props, _.keys(Component.propTypes));
}
