import React from 'react';

import _ from 'lodash';

import PropTypes from 'prop-types';

import hoistStatics from 'hoist-non-react-statics';

import hoistProps from './hoist-props';

/**
 * withAsyncState - Description
 *
 * @param {type} Component           Description
 * @param {type} AsyncWaitComponent  Description
 * @param {type} AsyncErrorComponent Description
 * @param {type} check               Description
 *
 * @return {type} Description
 */
export default function withAsyncState(
  Component,
  AsyncWaitComponent,
  AsyncErrorComponent,
  check
) {
  /**
   * withAsyncState - Description
   */
  class withAsyncState extends React.Component {
    /**
     * @static displayName - Description
     *
     * @return {type} Description
     */
    static get displayName() {
      return `withAsyncState(${Component.displayName || Component.name})`;
    }

    /**
     * @static WrappedComponent - Description
     *
     * @return {type} Description
     */
    static get WrappedComponent() {
      return Component;
    }

    /**
     * @static propTypes - Description
     *
     * @return {type} Description
     */
    static get propTypes() {
      return {
        error: PropTypes.instanceOf(Error),
        fetch: PropTypes.func.isRequired,
        wrappedComponentRef: PropTypes.func,
      };
    }

    /**
     * constructor - Description
     *
     * @return {type} Description
     */
    constructor() {
      super();

      //
      this.state = {
        isFetching: true,
        hasFetched: false,
        hasError: false,
      };
    }

    /**
     * componentWillMount - Description
     *
     * @return {type} Description
     */
    componentWillMount() {
      const {props, state} = this;
      const {fetch, error} = props;
      const hasProps = check(props, state);
      if (_.isFunction(fetch) && !hasProps) {
        fetch(props, state);
      } else if (hasProps) {
        this.setState({
          hasFetched: true,
          isFetching: false,
          hasError: !!error,
          lastFetchedOn: Date.now(),
        });
      }
    }

    /**
     * componentWillReceiveProps - Description
     *
     * @return {type} Description
     */
    componentWillReceiveProps(nextProps) {
      const {state} = this;
      const {hasFetched} = state;
      if (!hasFetched) {
        const hasProps = check(nextProps, state);
        const hasError = !!nextProps.error;
        if (hasProps || hasError) {
          this.setState({
            hasFetched: true,
            isFetching: false,
            hasError: hasError,
            lastFetchedOn: Date.now(),
          });
        }
      }
    }

    /**
     * render - Description
     *
     * @return {type} Description
     */
    render() {
      const {hasFetched, isFetching, hasError} = this.state;

      const {fetch} = this.props;

      if (hasFetched && !hasError) {
        return <Component {...hoistProps(this, Component)} />;
      } else if (isFetching) {
        return AsyncWaitComponent ? <AsyncWaitComponent /> : null;
      } else {
        return AsyncErrorComponent ? (
          <AsyncErrorComponent fetch={fetch} />
        ) : null;
      }
    }
  }

  return hoistStatics(withAsyncState, Component);
}
