import React, {Component} from 'react';

import PropTypes from 'prop-types';

import should from 'should';

import hoistProps from '../src/hoist-props';

import withAsyncState from '../src/index';

describe ('hoistProps(srcComponent, Component)', function () {


  /**
   * SourceComponentWithProps - Description
   * @extends Component
   */
  class SourceComponentWithProps extends Component {
    static get propTypes() {
      return {
        prop1: PropTypes.string.isRequired,
        prop2: PropTypes.string.isRequired,
      };
    }
  }


  /**
   * DestinationComponentWithProps - Description
   * @extends Component
   */
  class DestinationComponentWithProps extends Component {
    static get propTypes() {
      return {
        prop2: PropTypes.string.isRequired,
        prop3: PropTypes.number.isRequired,
      };
    }
  }


  it ('should be a function', function (done) {
    should(hoistProps).be.a.Function();
    done();
  });

  it ('should return an object with props from source Component', function (done) {

    hoistProps(
      <SourceComponentWithProps prop1="prop1" prop2="prop2" prop3={3}/>,
      DestinationComponentWithProps
    ).should.be.an.Object()
      .and.have.properties([
        'prop2', 'prop3',
      ]);

    done();
  });

});





describe ('withAsyncState()', function () {

  class Result extends Component {}
  class AsyncWaiting extends Component {}
  class AsyncError extends Component {}

  it ('should export a React high-order component', function (done) {

    should(withAsyncState).be.a.Function();
    done();

  });

  it ('should return a component', function (done) {

    console.log(
      withAsyncState(Result, AsyncWaiting, AsyncError)
    );
    done();

  });

});
